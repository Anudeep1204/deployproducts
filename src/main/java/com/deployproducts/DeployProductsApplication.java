package com.deployproducts;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.deployproducts.services.ProductService;

@SpringBootApplication
public class DeployProductsApplication implements CommandLineRunner{
	
	@Resource
	ProductService productService; 

	public static void main(String[] args) {
		SpringApplication.run(DeployProductsApplication.class, args);
	}
	
	@Override
	public void run(String... arg) throws Exception {
		productService.init();
	}
}
